#include <opencv2/opencv.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv/cvaux.hpp>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string.h>

using namespace cv;
using namespace std;

const float scale_factor(1.2);
const int min_neighbors(3);
const string inputfilename="/Users/bingyushen/Downloads/evaluation/FDDB-folds/FDDB-fold-01.txt"; //global variant to provide convenience for changing directory to get results for different folds.
const string outputfilename="/Users/bingyushen/Desktop/biometrics/outputfolder/fold-01-out.txt";//output file name
static string outputvalue[200]={""}; //for contentc written in the output file, like the cooridinate of rectangle, width and height of rectangle, and detection score.


void facedetection(string filename, int mode) //select cascade by input parameter， input location of picture to be processed
{
    cv::CascadeClassifier cascade; //create cascade object
    switch (mode) {
        case 1:
            cascade.load("/Applications/opencv-3.0.0/data/haarcascades/haarcascade_frontalcatface.xml");//Loads a classifier from a file
            break;
        case 2:
            cascade.load("/Applications/opencv-3.0.0/data/lbpcascades/lbpcascade_frontalface.xml");
            break;
        case 3:
            cascade.load("/Applications/opencv-3.0.0/data/haarcascades/haarcascade_frontalface_alt2.xml");
            break;
            
        default:
            cascade.load("/Applications/opencv-3.0.0/data/haarcascades/haarcascade_frontalface_alt2.xml");
            break;
    }
    //if (cascade.load("/Applications/opencv-3.0.0/data/lbpcascades/lbpcascade_frontalface.xml")) {
    
    // for (int i=1;i<argc; i++) {
    
    cv::Mat img=cv::imread(filename, CV_LOAD_IMAGE_GRAYSCALE);
    
    equalizeHist(img,img); //histogram equalization
    vector<Rect> objs;
    vector<int> reject_levels;
    vector<double> level_weights;
    
    //Detects objects of different sizes in the input image. The detected objects are returned as a list of rectangles.
    //Parameter objs: Vector of rectangles where each rectangle contains the detected object.
    //Parameter scale_factor: specifying how much the image size is reduced at each image scale.
    //Parameter min_neighbors: specifying how many neighbors each candidate rectangle should have to retain it.
    //Parameter minSize: Minimum possible object size. Objects smaller than that are ignored.
    //Parameter maxSize: Maximum possible object size. Objects larger than that are ignored.
    cascade.detectMultiScale(img,objs,reject_levels,level_weights,scale_factor,min_neighbors,0,Size(),Size(),true);
    //groupRectangles(objs,10);
    cv::Mat img_color=cv::imread(filename,CV_LOAD_IMAGE_COLOR);
    
    //ofstream out3(outputfilename);
    //out3<<objs.size()<<endl;
    
    outputvalue[0]=to_string(objs.size()); //write number of faces detected in the picture
    
    for (int n=0; n<objs.size(); n++) {
        
        rectangle(img_color,objs[n],Scalar(255,255,0),2,8,0);
        putText(img_color, std::to_string(level_weights[n]), Point(objs[n].x,objs[n].y), 1, 1, Scalar(0,0,255));
        string writetofile=to_string(objs[n].x)+" "; // write what's written for one detected fave in a string
        writetofile+=to_string(objs[n].y)+" ";
        writetofile+=to_string(objs[n].width)+" ";
        writetofile+=to_string(objs[n].height)+" ";
        writetofile+=to_string(level_weights[n]);
        outputvalue[n+1]={writetofile};
    }
    
    imshow("VJ Face Detector",img_color);
    
    cv::waitKey(0); //wait until key stroke detected
    // }
    //}
    
}

int main()
{
    //user select parameter: which cascade (can be extended to more than 5 kinds of cascades)
    int xmlmode;
    cout<<"please select a cascade: "<<endl;
    cout<<"1 haarcascade_frontalface, "<<endl;
    cout<<"2 lbpcascade_frontalface, "<<endl;
    cout<<"3 haarcascade_frontalface_alt2"<<endl;
    
    cin>>xmlmode;
    
    ifstream fin(inputfilename);
    ofstream out2(outputfilename);
    string picname;
    
    string mydatasetname;
    for (int x=1; x<51; x++) {
        string filenamemain="/Users/bingyushen/Desktop/biometrics/mydataset/";
        mydatasetname+=to_string(x);
        mydatasetname+=".jpg";
        filenamemain+=mydatasetname;
        cout<<filenamemain<<endl;
        facedetection(filenamemain,xmlmode);
        mydatasetname="";
    }
    
    while( getline(fin,picname) )
    {
        int y;
        cout<<"stop?"<<endl;
        cin>>y;
        if (y==1) {
            return 0;
        }
        out2<<picname<<endl;
        cout<<picname<<endl; //write picture name to the output file as requested
        for (int i=0; i<100; i++) {
            outputvalue[i]=""; //clean the golbal array used for collect each face information in every picture
        }
        
        //generate picture directory
        string filenamemain="/Users/bingyushen/Downloads/evaluation/originalPics/";
        picname+=".jpg";
        filenamemain+=picname;
        
        facedetection(filenamemain,xmlmode);
        picname="";
        
        //write data collected into output file
        for (int k=0; outputvalue[k]!=""; k++) {
            out2<<outputvalue[k]<<endl;
        }
    }
    
    return 0;
    
}



